#!/bin/bash

git pull || git stash; git pull

pkill -f "godot --path ./InfraServer/ --headless"

./Godot/download.sh

rm -r ./InfraServer/.godot/

./Godot/godot --path ./InfraServer/ --headless


