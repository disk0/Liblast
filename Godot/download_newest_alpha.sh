#!/bin/bash

wget https://archive.hugo.pro/builds/godot/master/editor/godot-linux-nightly-x86_64.zip &
wget https://archive.hugo.pro/builds/godot/master/editor/godot-windows-nightly-x86_64.zip &
wget https://archive.hugo.pro/builds/godot/master/editor/godot-macos-nightly-x86_64.dmg &
wget https://archive.hugo.pro/builds/godot/master/templates/godot-templates-android-html5-linux-windows-nightly.tpz &
wget https://archive.hugo.pro/builds/godot/master/templates/godot-templates-ios-macos-nightly.tpz


