#!/bin/bash

SDIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
README=$SDIR/../README.adoc

# Function to remove all existing Godot editor files
clean_up_old_stuff() {
	if [ -f "$SDIR/godot*" ]; then
		echo "Removing existing Godot files."
		rm -v "$SDIR/godot*"
	fi

	# Delete all disposable files related to the project including import and shader cache
	rm -rvf ../Game/.godot/
	rm -rvf ~/.cache/godot/
	rm -rvf ~/.config/godot/projects/Liblast*
	rm -rvf ~/.local/share/godot/app_userdata/Liblast*
}

if [ "$(uname -s)" == Linux ]; then
	clean_up_old_stuff

	# downlaod and extract the Linux build
	curl --location https://unfa.xyz/liblast/godot/godot-linux-x86_64.zip --output ./godot-linux-x86_64.zip && unzip godot-linux-x86_64.zip

	#stop execution here
	exit

	# Check if an update is needed
	# ripgrep (rg) is needed for the check
	if [ -f "$SDIR"/godot ] && command -v rg &>/dev/null; then
		LATEST_VERSION=$(rg 'version used in `main` branch is `(.+)`' -r '$1' -o "$README")
		CURRENT_VERSION=$("$SDIR"/godot --version)

		if [ "$LATEST_VERSION" == "$CURRENT_VERSION" ]; then
			echo "Current version matches the version from the README. No need to update!"
			exit
		fi
	fi

	# Download latest build
	TMP=$(mktemp)
	curl --location --output "$TMP" "$(grep "https://unfa.xyz/liblast/godot/godot-linux.*\.zip" -o "$README")"

	# Extract
	unzip -d "$SDIR" "$TMP"

	# Clean up
	rm "$TMP"

	echo "Done."
else
	clean_up_old_stuff

	echo "OS is not Linux."
	read -p "Enter w if you are on Windows or m if you are on MacOS [w/m]: " INPUT
	if [[ $INPUT == "w" || $INPUT == "m" ]]; then
		curl --location --continue-at - --remote-name --output-dir "$SDIR" "$(grep "https://unfa.xyz/liblast/godot/godot-$INPUT.*\...[pg]" -o "$README")"
		echo "Done."
	else
		echo "Invalid input."
	fi
fi
