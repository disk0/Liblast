extends Control

# These functions are for applying settings changes
func quit_game():
	get_tree().get_multiplayer().multiplayer_peer = null
	get_tree().quit()
