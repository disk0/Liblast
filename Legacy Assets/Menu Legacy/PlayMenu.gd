extends "res://Assets/Menu/Menu.gd"


func on_visibility_changed():
	if visible:
		$SuicideButton.visible = (MultiplayerState.role != Globals.MultiplayerRole.NONE)

func commit_suicide():
	Main.commit_suicide()

func host_button_pressed():
	Main.host_server()
 
func _on_disconnect_button_pressed():
	Main.disconnect_self()
