# Tracks the player input so it can be replicated to the server.
extends Node

# Holds the action state as paresed/received from the player
# We can use a setter to do further validation (e.g. clamping real values).
enum Action {LEFT, RIGHT, UP, DOWN, MAX}
var state : Array[bool] = []

# We can further use this node to send RPCs as authority (or do it in a separate node)
enum Ward {HEALING_WARD}
signal ward_placed(ward_type: int)


func _init():
	state.resize(Action.MAX)
	state.fill(false)


func _enter_tree():
	# Ensure the state is only visibile to the server (not replicated to other clients).
	# When needed, a subset of the state can be replicated via a different synchronizer.
	$InputSync.public_visibility = false
	$InputSync.set_visibility_for(1, true)


# Called during spawn, to set the proper authority for this node, and the synchronizer.
func set_player(p_id: int):
	set_multiplayer_authority(p_id)
	$InputSync.set_multiplayer_authority(p_id)


# Only gather input if we are the authority
func _process(delta):
	if not multiplayer.has_multiplayer_peer() or not is_multiplayer_authority():
		return
	state[Action.LEFT] = Input.is_action_pressed(&"ui_left")
	state[Action.RIGHT] = Input.is_action_pressed(&"ui_right")
	state[Action.UP] = Input.is_action_pressed(&"ui_up")
	state[Action.DOWN] = Input.is_action_pressed(&"ui_down")

	if Input.is_action_just_pressed(&"ui_cancel"):
		# Meh, Godot should allow calling RPCs on self.
		if multiplayer.is_server():
			place_ward(Ward.HEALING_WARD)
		else:
			place_ward.rpc_id(1, Ward.HEALING_WARD)


# Will be called when a user asks to place a ward.
@rpc
func place_ward(ward_type: int):
	ward_placed.emit(ward_type)


# This is called to apply the input, normally by the server (but could be expandend
# to do prediction in the client.
func apply(node, delta):
	if state[Action.LEFT]:
		node.translate(Vector3.LEFT * delta * 4)
	elif state[Action.RIGHT]:
		node.translate(Vector3.RIGHT * delta * 4)
	elif state[Action.UP]:
		node.translate(Vector3.FORWARD * delta * 4)
	elif state[Action.DOWN]:
		node.translate(Vector3.BACK * delta * 4)
