extends Node3D

@onready var spawnpoints = $SpawnPoints

signal match_finished


func update_reflection_probes(var_name, value: bool) -> void:
	if var_name == "render_refprobes_enabled":
		get_node("ReflectionProbes").visible = value


# Called when the node enters the scene tree for the first time.
func _ready():
	Settings.var_changed.connect(update_reflection_probes)
	var tmpvar = Settings.get_var("render_refprobes_enabled")
	if tmpvar:
		get_node("ReflectionProbes").visible = tmpvar


func start_match():
	prints("Start match called on map", name)


func get_spawnpoint() -> Transform3D:
	return spawnpoints.get_child(randi() % spawnpoints.get_child_count()).global_transform
