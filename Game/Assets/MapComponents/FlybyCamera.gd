extends Path3D

@onready var follow := $FlybyCameraFollow
@onready var camera := follow.get_node("Camera")

@export var speed := 1

@export var smooth := 2.0

func _process(delta):
	camera.current = true

	# Animate the Camera movement along the path
	follow.progress += delta * speed
	camera.global_transform.origin = camera.global_transform.origin.slerp(follow.global_transform.origin, delta / smooth)
	camera.global_transform.basis = camera.global_transform.basis.orthonormalized().slerp(follow.global_transform.basis.orthonormalized(), delta / smooth)
