extends Control

const AVATAR_SIZE : int = pow(2, 7) # 128

@onready var store_online = $CenterContainer/VBoxContainer/HBoxContainer/StoreOnline
@onready var claim_name = $CenterContainer/VBoxContainer/ProfileContainer/DisplayNameContainer/ClaimName
@onready var display_name = $CenterContainer/VBoxContainer/ProfileContainer/DisplayNameContainer/DisplayName
@onready var display_color = $CenterContainer/VBoxContainer/ProfileContainer/ColorContainer/ColorPickerButton
@onready var apply = $CenterContainer/VBoxContainer/HBoxContainer/Apply
@onready var avatar_preview = $CenterContainer/VBoxContainer/ProfileContainer/Avatar/AvatarPreview

var something_changed := false:
	set(value):
		something_changed = value
		apply.disabled = not value

var avatar_changed := false
var avatar_awaiting_reply := false
var display_name_changed := false
var display_color_changed := false


var avatar_data : PackedByteArray
var avatar_hash : PackedByteArray


var local_profile_path = "user://user_profile.liblast"


func auth_changed(enabled):
	if enabled:
		claim_name.disabled = false
		claim_name.button_pressed = true

		store_online.disabled = false
		store_online.button_pressed = true

		# default to username
		display_name.text = MultiplayerState.auth_username
		display_color.color = Color.from_hsv(randf_range(0, 1), randf_range(0.25, 1), randf_range(0.25, 1))

	else:
		claim_name.disabled = true
		claim_name.button_pressed = false

		store_online.disabled = true
		store_online.button_pressed = false

		# generate a random display name
		display_name.text = "Player #" + str(randi() % 9999).pad_zeros(4)
		display_color.color = Color.from_hsv(randf_range(0, 1), randf_range(0.25, 1), randf_range(0.25, 1))


func infra_online():
	if MultiplayerState.auth_enabled:
		claim_name.disabled = false
		store_online.disabled = false


func infra_offline():
	claim_name.disabled = false
	store_online.disabled = false


func _ready():
	MultiplayerState.auth_changed.connect(auth_changed)
	InfraServer.peer.connection_failed.connect(infra_offline)
	InfraServer.peer.connection_succeeded.connect(infra_online)
	InfraServer.peer.server_disconnected.connect(infra_offline)

	load_local_profile()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_select_avatar_pressed():
	$CenterContainer/VBoxContainer/ProfileContainer/Avatar/SelectAvatar/FileDialog.show()


func _on_file_dialog_file_selected(path):
	var avatar = Image.load_from_file(path)
	var avatar_size = avatar.get_size()
	prints("Soure image dimensions:", avatar_size)
	if avatar_size.aspect() == 1: # is the image square?
		print("The image is square")
	else:
		print("Cropping non-square image")
		if avatar_size.x < avatar_size.y:
			avatar.crop(avatar_size.x, avatar_size.x)
		else:
			avatar.crop(avatar_size.y, avatar_size.y)

	# by this point the avatar has to be square

	if avatar.get_width() == AVATAR_SIZE: # is it of perfect size?
		print("The image is of perfect size")
	elif avatar.get_width() > AVATAR_SIZE: # is to too big?
		print("Shrinking the image")
		avatar.resize(AVATAR_SIZE, AVATAR_SIZE, Image.INTERPOLATE_LANCZOS)
	elif avatar.get_width() < AVATAR_SIZE:
		print("Image is too small!")

	print("Displaying avatar preview")

	var avatar_texture = ImageTexture.create_from_image(avatar)
	avatar_preview.texture = avatar_texture
#	print($Avatar/Panel/AvatarPreview.texture)

	print("Saving the processed avatar")

	avatar_data = avatar.save_webp_to_buffer()
	avatar_hash = Storage.hash_data(avatar_data)

	avatar_changed = true
	something_changed = true

	#avatar.save_webp("user://avatar.webp", true)


func set_avatar_from_data():
	var avatar = Image.new()
	avatar.load_webp_from_buffer(avatar_data)

	var avatar_texture := ImageTexture.create_from_image(avatar)

	if avatar_texture == null:
		print_debug("The avatar image data produced a null texture")
		return

	prints("Avatar texture:", var_to_str(avatar_texture))

	avatar_preview.texture = avatar_texture


func upload_avatar():
	var request = [
	"user_update_avatar",
	{
		"peer_id" = InfraServer.peer.get_unique_id(),
		"username_hash" = MultiplayerState.auth_username.sha256_text(),
		"token" = MultiplayerState.auth_tokens[0],
	},
	{
		"data" = avatar_data,
		"hash" = avatar_hash,
	},
	]

	var err = InfraServer.peer.put_var(request)

	avatar_awaiting_reply = true

	if err == OK:
		prints("Avatar update request sent.")
	else:
		print("Error sending avatar update request:",err)


func request_avatar():
	prints("Requesting avatar hash:", avatar_hash)

	var request = [
		"retrieve_data",
		{
			"peer_id" = InfraServer.peer.get_unique_id(),
			"username_hash" = MultiplayerState.auth_username.sha256_text(),
			"hash" = avatar_hash
		},
	]
	avatar_awaiting_reply = true
	InfraServer.peer.put_var(request)


func _on_apply_pressed():
	if store_online.button_pressed: # store remotely?
		if avatar_changed:
			upload_avatar()

	if avatar_changed:
			var err = Storage.store(avatar_hash, avatar_data, MultiplayerState.auth_username.sha256_text())
			prints("Storing avatar data locally, result:", error_string(err))


	save_local_profile()

func _on_display_name_text_changed(new_text):
	display_name_changed = true
	something_changed = true
	MultiplayerState.user_character_profile.display_name = new_text


func _on_color_picker_button_color_changed(color):
	display_color_changed = true
	something_changed = true
	MultiplayerState.user_character_profile.display_color = color


func save_local_profile():
	var file = File.new()
	file.open(local_profile_path, File.WRITE)

	var buf = {
		"display_name" = MultiplayerState.user_character_profile.display_name,
		"display_color" = MultiplayerState.user_character_profile.display_color,
		"avatar_hash" = MultiplayerState.user_character_profile.avatar_hash,
		}
	file.store_var(buf)

	var err = file.get_error()
	if err != OK:
		prints("Can't save local user profile. Error:", error_string(err))


func load_local_profile():
	var file = File.new()
	file.open(local_profile_path, File.READ)
	var buf = file.get_var()

	var err = file.get_error()
	if err != OK:
		prints("Can't load local user profile. Error:", error_string(err))
		#save_local_profile()
	else:
		display_name.text = buf["display_name"]
		MultiplayerState.user_character_profile.display_name = buf["display_name"]

		display_color.color = buf["display_color"]
		MultiplayerState.user_character_profile.display_color = buf["display_color"]

		avatar_hash = buf["avatar_hash"]
		MultiplayerState.user_character_profile.avatar_hash = buf["avatar_hash"]


func _on_user_profile_visibility_changed():
	if visible:
		$Timer.start()

		if avatar_data.is_empty() and not avatar_hash.is_empty(): # gotta load the avatar!
			request_avatar()

	else:
		$Timer.stop()


func _on_timer_timeout():
	InfraServer.peer.poll()

	if InfraServer.peer.get_available_packet_count() > 0:
		var reply = InfraServer.peer.get_var()

		if reply[0] == "user_update_avatar" and avatar_awaiting_reply:
			if reply[1] == OK:
				print("Avatar change successful!")
				avatar_awaiting_reply = false
				avatar_changed = false
			elif reply[1] == ERR_INVALID_DATA:
				print("Avatar data got corrupted, resending...")
				upload_avatar()
			elif reply[1] == ERR_ALREADY_EXISTS:
				print("Uploaded avatar already exists on the server!")
				avatar_awaiting_reply = false
				avatar_changed = false
			else:
				prints("Error changing avatar:", error_string(reply[1]))
				avatar_awaiting_reply = false
		elif reply[0] == "retrieve_data" and avatar_data.is_empty() and avatar_awaiting_reply:
			if reply[1].has("data"):
				var data = reply[1]["data"]
				#var recieved_hash = reply[1]["hash"]
				var computed_hash = Storage.hash_data(data)
				if avatar_hash != computed_hash:
					print("Avatar data integrity check FAILED. Re-sending request")
					request_avatar()
				else:
					avatar_data = data
					print("Avatar downloaded succesfully")
					avatar_awaiting_reply = false
					# cache retrived data locally for later
					Storage.store(avatar_hash, data, MultiplayerState.auth_username.sha256_text())
					set_avatar_from_data()
			elif reply[1].has("error"):
				prints("Error downlaoding avatar:", error_string(reply[1]["error"]))
