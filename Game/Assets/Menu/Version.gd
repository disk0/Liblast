extends RichTextLabel

func _ready():
	Settings.check_version()
	text = Settings.version

	# text animation
	var tween = create_tween()
	visible_ratio = 0.0
	tween.tween_interval(0.5)
	tween.tween_property(self, "visible_ratio", 1.0, 2)
	tween.play()
