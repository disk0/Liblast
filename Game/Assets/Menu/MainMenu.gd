extends Control

var menu_map : Node3D:
	set(value):
		menu_map = value
		# pass the reference so the Game menu can free the menu bg map
		$TabContainer/Game.menu_map = menu_map

# Called when the node enterss the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
