extends Control

signal game_joined

const MAP_DIR = "res://Assets/Maps"

var selected_map_path : String = ""

var game_state : GameState

var menu_map : Node3D

@onready var mapsel : OptionButton = $CenterContainer/VBoxContainer/HBoxContainer3/MapSelection

func find_available_maps():
	mapsel.clear()
#	print("Searching for maps...")
	var dir = Directory.new()
	if dir.open(MAP_DIR) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
#				print("Found directory: " + file_name)
				pass
			else:
#				print("Found file: " + file_name)

				# only .TSCN files can contain maps.
				# Files with names beginnngin with `_` are special and should not be listed
				if file_name.ends_with(".tscn") and not file_name.begins_with('_'):
#					prints("Found map file:", file_name)
					mapsel.add_item(file_name, mapsel.item_count + 1)
			file_name = dir.get_next()
			# make sure something is selected always
			mapsel.select(1)
	else:
		print_debug("An error occurred when trying to access the map path.")

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	find_available_maps()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not selected_map_path.is_empty():
		var progress : Array
		var result = ResourceLoader.load_threaded_get_status(selected_map_path, progress)
#		print(progress)
		$CenterContainer/VBoxContainer/MapLoadingProgress.value = clamp(((progress[0] * 100) - 92) * 30, 0, 100)
		if result == ResourceLoader.THREAD_LOAD_LOADED:
			set_process(false)
			$CenterContainer/VBoxContainer/MapLoadingProgress.hide()

func _on_host_toggled(button_pressed):
	if button_pressed:
		# make sure there is a map path provided
		if selected_map_path.is_empty():
			selected_map_path = MAP_DIR.path_join(mapsel.get_item_text(mapsel.selected))

		MultiplayerState.start_server()
		#await(MultiplayerState.ServerStarted)
		prints("Setting map path:", selected_map_path)
		MultiplayerState.game_state.map_path = selected_map_path
		print("Showing progress")
		$CenterContainer/VBoxContainer/MapLoadingProgress.show()
		set_process(true)
		print("Waiting for map")
		await(MultiplayerState.game_state.map_loaded)
		print("Map loaded")
		if menu_map != null:
			menu_map.queue_free()
#		game_state = preload("res://Assets/Game/GameState.tscn").instantiate()
#		game_state.name = "GameState"
#		get_tree().root.add_child(game_state)
#		MultiplayerState.game_state = game_state
	else:
		MultiplayerState.stop_server()

func _on_join_toggled(button_pressed):
	if $CenterContainer/VBoxContainer/HBoxContainer3/Host.button_pressed:
		return

	if button_pressed:
		MultiplayerState.start_client($CenterContainer/VBoxContainer/HBoxContainer/HostAddress.text)
		print("Showing progress")
		$CenterContainer/VBoxContainer/MapLoadingProgress.show()
		set_process(true)
		print("Waiting for map")
		await(MultiplayerState.game_state.map_loaded)
		print("Map loaded")
		if menu_map != null:
			menu_map.queue_free()
	else:
		MultiplayerState.stop_client()


	#game_state.spawn_character(true)


func _on_map_selection_item_selected(index):
	selected_map_path = MAP_DIR.path_join(mapsel.get_item_text(mapsel.selected))
