extends LinkButton

@export var url = "https://libla.st"

func _on_liblast_link_pressed():
	OS.shell_open(url)
