extends "res://Assets/Characters/CharacterController.gd"

var aim_noise_offset := randi()

# A Bot need to keep track of the controls by type, hence using a dictionary
# rather than an array as in the case of a Player
var bot_controls = {
	Globals.CharCtrlType.MOVE_F: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_B: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_L: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_R: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_S: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_J: CharCtrl.new(),
	Globals.CharCtrlType.TRIG_P: CharCtrl.new(),
	Globals.CharCtrlType.TRIG_S: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_1: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_2: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_3: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_L: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_R: CharCtrl.new(),
	Globals.CharCtrlType.V_ZOOM: CharCtrl.new(),
}

const character_profiles = [
	preload("res://Assets/Characters/CharacterProfiles/Bot1.tres"),
	preload("res://Assets/Characters/CharacterProfiles/Bot2.tres"),
	preload("res://Assets/Characters/CharacterProfiles/Bot3.tres"),
	]

func _ready():
#	print("Bot character profiles: ", var_to_str(character_profiles))
#
#	character.character_profile = character_profiles[randi() % character_profiles.size()]
#	character._apply_character_profile()

	# controls are missing control_type
	for ctrl in bot_controls.keys():
		var type = ctrl
		bot_controls[type].control_type = type

	# AI characters do not assume 1st person view by default
	character.first_person = false

func _input(event) -> void:
	# give the parent class a chance to start replay capture/pplayback
	control_replay(event)

	# the Bot does not process any user input

# simulated mouse aiming
func _process(delta):
	var noise = FastNoiseLite.new()
	noise.noise_type = FastNoiseLite.TYPE_SIMPLEX
	noise.frequency = 0.01
	noise.domain_warp_enabled = true
	noise.domain_warp_amplitude = 0.5
	noise.fractal_octaves = 4
	noise.fractal_gain = 0.5
	var aim : Vector2

	aim.x = noise.get_noise_1d((Time.get_ticks_msec() / 100) + aim_noise_offset)
	aim.y = noise.get_noise_1d((Time.get_ticks_msec() / 100) + 10000 + aim_noise_offset)

	var new_event := CharCtrlEvent.new()
	new_event.aim = aim

	_event_index += 1
	new_event.index = _event_index
	new_event.frame = Engine.get_physics_frames()

	emit_signal(&'CharControllerEvent', new_event)

func _on_timer_timeout() -> void:
	if replay_playback:
		return

	var new_event := CharCtrlEvent.new()
	_control_changed = false

	for control in bot_controls:
		if bot_controls[control].enabled:
			var cc1 := CharCtrlChange.new(bot_controls[control].control_type)
			cc1.enabled = false
			cc1.changed = true
			new_event.control_changes.append(cc1)
			_control_changed = true
			bot_controls[control].enabled = false

	if randf() < 0.75: # probability of taking action
		var cc2 := CharCtrlChange.new(randi_range(1,7))
		bot_controls[cc2.control_type].enabled = true
		cc2.enabled = true
		cc2.changed = true
		new_event.control_changes.append(cc2)

		_control_changed = true

	_event_index += 1
	new_event.index = _event_index
	new_event.frame = Engine.get_physics_frames()


	new_event.abs_location = character.global_transform.origin
	new_event.abs_aim.x = character.get_rotation().y # body left/right
	new_event.abs_aim.y = character.head.get_rotation().x # head up/down

	$Timer.wait_time = randf_range(0.25, 2)

	if _control_changed:
		emit_signal(&'CharControllerEvent', new_event)

		if replay_capture:
			if replay.size() == 0:
				print("Recording first frame")
				new_event.use_absolute = true # to reset position and aim for replay playback

			print("Bot recorded event: ", new_event.index)
			replay.append(new_event)
