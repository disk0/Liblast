class_name CharMovement

# Character movement may need to pass something up to HUD
signal character_hud_update(update: CharHudUpdate)

var character: CharacterBody3D

enum Medium {AIR, GROUND, WATER}
var medium: Medium = Medium.GROUND

var accel_type := {
	Medium.GROUND: 12,
	Medium.AIR: 1,
	Medium.WATER: 4,
	}

var speed_type := {
	Medium.GROUND: 10,
	Medium.AIR: 10,
	Medium.WATER: 5,
	}

var damp_type := {
	Medium.GROUND: 0,
	Medium.AIR: 0.01,
	Medium.WATER: 0.05,
	}

var gravity: float = 28
var jump: float = 14

var gravity_vec: Vector3

var walk_dir: Vector3

var walk_speed: float = 0
var walk_accel: float = 0
var walk_damp: float = 0

var movement_velocity := Vector3.ZERO
var previous_velocity := Vector3.ZERO

var special_type = "jetpack"
var jetpack_thrust: float = 48 # force applied against gravity
var jetpack_tank: float = 0.5 # maximum fuel
var jetpack_fuel: float = jetpack_tank # current fuel
var jetpack_recharge: float = 0.25 # how long to recharge to full
var jetpack_min: float = 0.125 # cannot deploy jetpack if fluel is below this
var jetpack_active: bool:
	set(value):
		# ignore duplicated requests
		if value == jetpack_active:
			return

		jetpack_active = value
#		prints("Changing jetpack to", value)

		var sound = character.get_node("Jetpack/Sound")
		if sound:
			if value:
				sound.play(randf_range(0, sound.stream.get_length()))
			else:
				sound.stop()

		var light = character.get_node("Jetpack/Light")
		if light:
			light.visible = value

		var trail = character.get_node("Jetpack/Jet")
		if trail:
			trail.visible = value

func aim(aim: Vector2) -> void:
	# body left/right
	character.rotate_y(aim.x)
	# head up/down
	character.head.rotation.x = clamp(character.head.rotation.x + aim.y, -PI/2, PI/2)

func process(delta:float) -> void:

	# Gravity
	if character.is_on_floor():
		gravity_vec = Vector3.ZERO
	else:
		gravity_vec += Vector3.DOWN * gravity * delta

	# Air control
	if character.is_on_floor():
		medium = Medium.GROUND
	else:
		medium = Medium.AIR

	# Jumping
	if character.controls[Globals.CharCtrlType.MOVE_J].changed \
	and character.controls[Globals.CharCtrlType.MOVE_J].enabled \
	and character.is_on_floor():
			gravity_vec += Vector3.UP * jump

	### Jetpack

	# turning jetpack on and off
	if character.controls[Globals.CharCtrlType.MOVE_S].changed: # changed gets reset whenever we check it
		if character.controls[Globals.CharCtrlType.MOVE_S].enabled \
		and not jetpack_active and jetpack_fuel >= jetpack_min:
			jetpack_active = true
		elif not character.controls[Globals.CharCtrlType.MOVE_S].enabled \
		and jetpack_active \
		or jetpack_fuel == 0:
			jetpack_active = false

	# discharging
	if jetpack_active and jetpack_fuel > 0:
		gravity_vec += Vector3.UP * jetpack_thrust * delta
		jetpack_fuel = max(jetpack_fuel - delta, 0)

		var update = CharHudUpdate.new()
		update.special = jetpack_fuel / jetpack_tank
		update.special_type = special_type
		emit_signal(&'character_hud_update', update)
	# running empty
	elif jetpack_fuel == 0:
#		print("Jetpack empty")
		jetpack_active = false

	# recharging
	if not jetpack_active and jetpack_fuel < jetpack_tank:
		jetpack_fuel = min(jetpack_fuel + jetpack_recharge * delta, jetpack_tank)

		var update = CharHudUpdate.new()
		update.special = jetpack_fuel / jetpack_tank
		update.special_type = special_type
		emit_signal(&'character_hud_update', update)

	# Walking direction
	walk_dir = Vector3.ZERO
	if character.controls[Globals.CharCtrlType.MOVE_F].enabled:
		walk_dir -= character.transform.basis.z
	if character.controls[Globals.CharCtrlType.MOVE_B].enabled:
		walk_dir += character.transform.basis.z
	if character.controls[Globals.CharCtrlType.MOVE_L].enabled:
		walk_dir -= character.transform.basis.x
	if character.controls[Globals.CharCtrlType.MOVE_R].enabled:
		walk_dir += character.transform.basis.x

	if walk_dir.length() > 0: # normalized() will return a null
		walk_dir = walk_dir.normalized()

	# Walk velocity
	walk_speed = speed_type[medium]
	walk_accel = accel_type[medium]
	walk_damp = damp_type[medium]

	# movement acceleration/deceleration
	movement_velocity = movement_velocity.lerp(walk_dir * walk_speed, walk_accel * delta)
	previous_velocity = character.velocity

	# sum movement velocity and gravity; apply damping
	character.velocity = (movement_velocity + gravity_vec).lerp(Vector3.ZERO, walk_damp)
	# Perform movement
	character.move_and_slide()

	# Preserve momentum after collision while in air
	if not character.is_on_floor():
		movement_velocity.x = character.velocity.x
		movement_velocity.z = character.velocity.z
		gravity_vec.y = character.velocity.y
