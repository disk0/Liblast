extends Resource
class_name CharLoadout

# Holds information about weapons a character is equipped with

@export var primary : Weapons.Weapon
@export var secondary : Weapons.Weapon
@export var melee : Weapons.Weapon
