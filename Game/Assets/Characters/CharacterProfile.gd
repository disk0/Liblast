extends Resource
class_name CharacterProfile


# This class contains information about character appearance customization
# and other personalized stuff that will be visible to other players in-game

# character display name (scoreboard, chat etc.)
@export var display_name : String = "Default"
# character color (chat, scoreboard name color, character model accents etc.)
@export var display_color : Color = Color.DARK_GRAY
# character custom face texture
@export var face : Texture2D = load("res://Assets/Characters/Faces/FacePlaceholder.png")

@export var avatar_hash := PackedByteArray()

# 1st person vertical FOV
@export var fov : int = 90

@export var badges : Array[Badges.Badge]

#enum Badge {
#	DEVELOPER,
#	CONTRIBUTOR,
#	SUPPORTER,
#	ADMIN,
#	MODERATOR,
#	PRE_ALPHA,
#	ALPHA,
#	BETA,
#	DAY_ONE,
#	BOT,
#	ERROR,
#}
#
#const badge_textures = {
#	Badge.DEVELOPER : preload("res://Assets/Badges/Badge_Developer.png"),
#	Badge.CONTRIBUTOR : preload("res://Assets/Badges/Badge_Contributor.png"),
#	Badge.SUPPORTER : preload("res://Assets/Badges/Badge_Supporter.png"),
#	Badge.ADMIN : preload("res://Assets/Badges/Badge_Admin.png"),
#	Badge.MODERATOR : preload("res://Assets/Badges/Badge_Moderator.png"),
#	Badge.PRE_ALPHA : preload("res://Assets/Badges/Badge_PreAlpha.png"),
#	Badge.ALPHA : preload("res://Assets/Badges/Badge_Alpha.png"),
#	Badge.BETA : preload("res://Assets/Badges/Badge_Beta.png"),
#	Badge.DAY_ONE : preload("res://Assets/Badges/Badge_DayOne.png"),
#	Badge.BOT : preload("res://Assets/Badges/Badge_Bot.png"),
#	Badge.ERROR : preload("res://Assets/Badges/Badge_Error.png")
#}
#
#@export_flags(
#"Liblast Developer",
#"Liblast Contributor",
#"Liblast Supporter",
#"Server Administrator",
#"Server Moderator",
#"Pre-Alpha Tester",
#"Alpha Tester",
#"Beta Tester",
#"Day One Player",
#"Bot",
#"ERROR"
#) var badges = 0
