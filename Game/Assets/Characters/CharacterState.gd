class_name CharacterState

var health: int
var alive: bool = true
var team: int = 0
var score: int

#var user_id =

# spawning
var spawn_time: float # scheduled (re)spawn time
var spawn_location: Transform3D # scheduled (re)spawn location

# connection stats
var ping: float
var packet_loss: float
