extends Node
class_name CharWeapons

const SWITCH_DOWN_DURATION = 0.1 # time to put away current weapon
const SWITCH_UP_DURATION = 0.2 # time to take out new weapon

var character : CharacterBody3D
var weapons_root : Node3D:
	set(value):
		weapons_root = value
		#print_debug(value)
		initialize()

var current : Weapon
var switching : bool # if this is true, weapons can't be switched or controlled
var last : Weapon

var primary : Weapon
var secondary : Weapon
var melee : Weapon

func initialize():
	if character.loadout == null:
		print_debug("Missing loadout")
		return

	var loadout : CharLoadout = character.loadout

	if loadout.primary == Weapons.Weapon.NONE or loadout.primary == null:
		print_debug("Primary weapon is none")
	else:
		var primary_resource = load(Weapons.WeaponScenePaths[loadout.primary])
		primary = primary_resource.instantiate()
		weapons_root.add_child(primary)
		primary.global_transform = weapons_root.global_transform
		primary.character = character
		#primary.is_current = true
		switch_weapon(primary)

	if loadout.secondary == Weapons.Weapon.NONE or loadout.secondary == null:
		print_debug("Secondary weapon is none")
	else:
		var secondary_resource = load(Weapons.WeaponScenePaths[loadout.secondary])
		secondary = secondary_resource.instantiate()
		weapons_root.add_child(secondary)
		secondary.global_transform = weapons_root.global_transform
		secondary.character = character
		#secondary.is_current = false

	if loadout.melee == Weapons.Weapon.NONE or loadout.melee == null:
		print_debug("Melee weapon is none")
	else:
		var melee_resource = load(Weapons.WeaponScenePaths[loadout.melee])
		melee = melee_resource.instantiate()
		weapons_root.add_child(melee)
		melee.global_transform = weapons_root.global_transform
		melee.character = character
		#melee.is_current = false

func switch_weapon(weapon: Weapon):
	if weapon == current:
		print_debug("Trying to switch to the current weapon")
	else:
#		print("Switching to weapon: ", weapon.name)

		# weapon switching animation
		var position_up = weapons_root.position
		var position_down = position_up + Vector3.DOWN
		var tween = weapons_root.create_tween()

		if current:
			switching = true
			tween.tween_property(weapons_root, "position", position_down, SWITCH_DOWN_DURATION).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_SINE)
			await tween.finished
			current.is_current = false
			last = current

		#weapon.is_current = true
		current = weapon
		var tween2 = weapons_root.create_tween()
		tween2.tween_property(weapons_root, "position", position_up, SWITCH_UP_DURATION).from(position_down).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_SINE)
		await tween2.finished
		switching = false


func _controller_event(event: CharCtrlEvent) -> void:
	if current and not switching:
		current._controller_event(event)

func process(delta):
	if current and not switching:
		current.process(delta)
#	else:
#		print("Current weapon is null")

	# can't switch weapons if previous switch didn't finish
	if not switching:
		# selected primary weapon
		if character.controls[Globals.CharCtrlType.WEPN_1].changed and \
			character.controls[Globals.CharCtrlType.WEPN_1].enabled:
#				print("Switching to primary weapon")
				switch_weapon(primary)

		# selected secondary weapon
		if character.controls[Globals.CharCtrlType.WEPN_2].changed and \
			character.controls[Globals.CharCtrlType.WEPN_2].enabled:
#				print("Switching to secondary weapon")
				switch_weapon(secondary)

		# selected melee weapon
		if character.controls[Globals.CharCtrlType.WEPN_3].changed and \
			character.controls[Globals.CharCtrlType.WEPN_3].enabled:
#				print("Switching to melee weapon")
				switch_weapon(melee)

		# selected last used weapon
		if character.controls[Globals.CharCtrlType.WEPN_L].changed and \
			character.controls[Globals.CharCtrlType.WEPN_L].enabled and \
			last != null:
#				print("Switching to last used weapon")
				switch_weapon(last)
