class_name CharCtrl

func _init(control_type = Globals.CharCtrlType.UNDEFINED, action : StringName = &'') -> void:
	if control_type:
		self.control_type = control_type
	
	if action != &'':
		self.action = action
	
	#print("initializing CharCtrl of type ", control_type, " with action: ", null)

var control_type : Globals.CharCtrlType

var action : StringName
var changed : bool

# evaluate this control's input
func get_control_change() -> CharCtrlChange:
	#print("get_input_change, action: ", var_to_str(action), "; enabled: ", enabled, "; changed: ", changed, "; ctrl_type: ", control_type)
	assert(self.action is StringName, "CharacterControl has NO assigned action. Cannot check input change!")
	assert(self.action != &"", "CharacterControl has an EMPTY assigned action. Cannot check input change!")
	var control_change = CharCtrlChange.new(control_type)
	if Input.is_action_just_pressed(self.action):
		control_change.enabled = true
		control_change.changed = true
	elif Input.is_action_just_released(self.action):
		control_change.enabled = false
		control_change.changed = true
	else:
		control_change.changed = false
	self.enabled = control_change.enabled
	self.changed = control_change.changed
	return control_change
		
var enabled : bool:
	set(value):
		if self.enabled != value:
			enabled = value
			changed = true

	get:
		self.changed = false
		return enabled
