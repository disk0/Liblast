class_name Character
extends CharacterBody3D

# used to update HUD if the character is being currently controlled or spectated
signal character_hud_update(update: CharHudUpdate)

# resource containg information about how the player character should look
@export var profile : Resource

# object containing current state of the character that needs to be updated between peers
@onready var state = CharacterState.new()

@export var controller_scene: PackedScene
var controller#: CharController # provides control input

@export var loadout : Resource

var weapons := CharWeapons.new()

var movement := CharMovement.new() # processes movement inputs

# TODO

@export var voice: Resource # handles voice for various character events
#var sounds: CharSounds # handles SFX for various character events

@export_node_path var head_path: NodePath
@onready var head: Node3D = self.get_node(head_path)

@export_node_path var mouth_path: NodePath
@onready var mouth: Node3D = self.get_node(mouth_path)

@export_node_path var camera_path: NodePath
@onready var camera: Camera3D = self.get_node(camera_path)

@export_node_path var hands_path: NodePath
@onready var hands: Node3D = self.get_node(hands_path)

@export_node_path var models_path: NodePath
@onready var models: Node3D = self.get_node(models_path)

@export_node_path var camera3rdPerson_path: NodePath
@onready var camera3rdPerson: Camera3D = self.get_node(camera3rdPerson_path)

@export_node_path var models3rdPerson_path: NodePath
@onready var models3rdPerson: Node3D = self.get_node(models3rdPerson_path)

@export_node_path var body_mesh_path: NodePath
@onready var body_mesh: MeshInstance3D = self.get_node(body_mesh_path)

@export_node_path var face_mesh_path: NodePath
@onready var face_mesh: MeshInstance3D = self.get_node(face_mesh_path)

@export_node_path var banner_path: NodePath
@onready var banner: Node3D = self.get_node(banner_path)

# binary controls
var controls = {
	Globals.CharCtrlType.MOVE_F: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_B: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_L: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_R: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_S: CharCtrl.new(),
	Globals.CharCtrlType.MOVE_J: CharCtrl.new(),
	Globals.CharCtrlType.TRIG_P: CharCtrl.new(),
	Globals.CharCtrlType.TRIG_S: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_1: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_2: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_3: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_L: CharCtrl.new(),
	Globals.CharCtrlType.WEPN_R: CharCtrl.new(),
	Globals.CharCtrlType.V_ZOOM: CharCtrl.new(),
}

@export var max_health: int = 100
#var health: int = max_health # spawn with max health

# used by console, chat and menu to make sure inputs ar not processed
var is_controllable: bool = false # previously "input_active"
var is_mobile: bool = false

func set_team(team:int) -> void:
	state.team = team
	body_mesh.set("shader_params/team", team)
	banner.get_node("NameTag").modulate = Globals.team_colors[team]

# 0 = no team, 1 = purple, 2 = lime
#var team: int:
#	set(value):
#		# ensure value within range
#		team = clampi(value,0,2)


# control 1st vs 3rd person view. CharControllers should set this in _ready()
var first_person: bool:
	set(value):
#		print_debug("Setting first_person to ", value, " on character ", profile.display_name)
		first_person = value

		# is this the currently watched character (not the same locally controlled one!)
		if Globals.current_character == self:
			if first_person:
				camera.current = true
				camera3rdPerson.current = false
			else:
				camera3rdPerson.current = true
				camera.current = false

		# now setup model visibility
		if first_person:
			models3rdPerson.hide()
			models.show()
			banner.hide()
			$Jetpack.transparency = 1
		else:
			models3rdPerson.show()
			models.hide()
			banner.show()
			$Jetpack.transparency = 0

#var revenge_pid: int #store PID of the player who killed you recently

func _enter_tree():
	# Ensure the state is only visibile to the server (not replicated to other clients).
	# When needed, a subset of the state can be replicated via a different synchronizer.
	$CharacterAuthority/StateSynchronizer.public_visibility = false
	$CharacterAuthority/StateSynchronizer.set_visibility_for(1, true)


func set_character_owner(pid: int):
	name = str(pid)
	profile.display_name = str(pid)
	set_multiplayer_authority(pid)
	$CharacterAuthority/StateSynchronizer.set_multiplayer_authority(pid)

# pass though HUD updates from subcomponents
func _character_hud_update(update: CharHudUpdate) -> void:
	emit_signal(&"character_hud_update", update)


func _ready():

	# if the character belongs to the local player
	if str(multiplayer.get_unique_id()) == name:
		print("Character belongs to the local player")
		MultiplayerState.local_character = self
		Globals.current_character = self
		first_person = true

	weapons.character = self
	weapons.weapons_root = hands

	controller = controller_scene.instantiate()
	# pass necessary reference to the character controller scene
	controller.character = self
	controller.global_transform = head.global_transform
	head.add_child(controller)
	controller.connect(&'CharControllerEvent', _controller_event)
	movement.connect(&'character_hud_update', _character_hud_update)

	# controls are missing control_type
	for ctrl in controls.keys():
		var type = ctrl
		controls[type].control_type = type

	# pass necessary reference to the character movement object
	movement.character = self

	_apply_profile()

	# TESTING: randomize team
	set_team(randi_range(1,2))
	state.health = max_health

	# enable movement
	get_tree().create_timer(1).timeout.connect(func (): is_mobile = true; is_controllable = true)

func _process(delta):
	weapons.process(delta)


@rpc func die() -> void:
	if state.health > 0:
		state.health = 0
	state.alive = false
	mouth.stream = voice.die
	mouth.play()


func respawn(RespawnLocation) -> void:
	pass


# function to apply visual changes using this character's profile resource
func _apply_profile() -> void:

	if profile == null:
		push_error("Attempting to apply a null character profile")
		return

#	print("Applying character profile:") #, var_to_str(character_profile))

#	character_profile = load("res://Assets/Characters/CharacterProfiles/Bot Blue.tres")

	camera.fov = profile.fov

	# apply face texture if valid
	if profile.face != null:
#		print("Applying face texture")
		var mat := face_mesh.mesh.surface_get_material(0).duplicate()
		mat['albedo_texture'] = profile.face
		face_mesh.set_surface_override_material(0, mat)
#	else:
#		print("Skipping face texture")

	# apply body color if valid
	if profile.display_color != null:
#		print("Applying body color")
		var mat2 := body_mesh.mesh.surface_get_material(0).duplicate()
		mat2['albedo_color'] = profile.display_color
		body_mesh.set_surface_override_material(0, mat2)
#	else:
#		print("Skipping body color")

	banner.get_node("NameTag").text = profile.display_name

	if profile.badges.size() > 0:
#		print("Applying badges: ", profile.badges)
		var priorities = {}
		var min_priority = 1 << 32 # use a really high number to make sure anything will be lower than it
#		print ("Min priority: ", min_priority)
		for i in profile.badges:
#			print(Badges.resources)
			priorities[Badges.resources[i].priority] = i
			min_priority = min(min_priority, Badges.resources[i].priority)

#		print ("Badges by priority: ", priorities)

		banner.get_node("Badge").texture = Badges.resources[priorities[min_priority]].texture
		banner.get_node("Badge").show()
	else:
		banner.get_node("Badge").hide()


#func bool_change(change: int) -> bool:
#	if change == CharCtrlEvent.ControlChange.ENABLED:
#		return true
#	elif change == CharCtrlEvent.ControlChange.DISABLED:
#		return false


func _controller_event(event: CharCtrlEvent) -> void:
#	print("Controller event recieved on peer ", multiplayer.get_unique_id())

	# in a multiplayer game only the multiplayer authority can control a character
	if MultiplayerState.role != Globals.MultiplayerRole.NONE:
		if is_multiplayer_authority() == false:
			return

	# dead characteres don't move;
	if state.alive and is_controllable:
	#	print("CharCtrlEvent recieved: ", var_to_str(event))
		# calculate error of simulated movement in relation to absolute loc/rot data
		var aim_error := Vector2.ZERO
		var loc_error := Vector3.ZERO

		if event.abs_location:
			loc_error = global_transform.origin - event.abs_location
	#		print("loc_error: ", loc_error.length())

		if event.abs_aim:
			aim_error.x = get_rotation().y - event.abs_aim.x # body left/right
			aim_error.y = head.get_rotation().x - event.abs_aim.y # head up/down
	#		print("aim_error: ", aim_error.length())

		#print("index: ", event.index, "loc: ", global_transform.origin, " abs_loc: ", event.abs_location)
		#print("index: ", event.index, "; rot: ", Vector2(get_rotation().y, head.get_rotation().x), "; abs_rot: ", event.abs_aim)

		if event.use_absolute:
			if event.abs_location:
				global_transform.origin = event.abs_location

			if event.abs_aim:
	#			print("Event abs aim: ", event.abs_aim)
				head.set_rotation(Vector3(event.abs_aim.y, 0, 0)) # head up/down
				set_rotation(Vector3(0, event.abs_aim.x, 0)) # body left/right

		if event.aim:
				movement.aim(event.aim)
	#			self.rotate_y(event.aim.x) # body left/right
	#			head.rotate_x(event.aim.y) # head up/down

		for ctrl in event.control_changes:
			var type = ctrl.control_type
			controls[type].enabled = ctrl.enabled
			controls[type].changed = true

		weapons._controller_event(event)
	#	print("CharCtrlEvent binary encode/decode Test")
	#	print("index: ", event.index)
	#	print("aim:  ", event.aim)
		#print("event:  ", var_to_str(event))
	#	var event2 = CharCtrlEvent.new()
	#	event2.decode(event.encode())
	#	print("AFTER ENCODE/DECODE")
	#	print("index: ", event2.index)
	#	print("aim2: ", event2.aim)

	#	var encoded_event = event.encode()
		#print("Encoded event: ", encoded_event )
	#	var decoded_event = CharCtrlEvent.new()
	#	decoded_event.decode(encoded_event)

		#print("Decoded event: ", var_to_str(decoded_event))

	#	if event == event2:
	#		print("ENCODING OK")
	#	else:
	#		print("ENCODING ERROR!")
	#		print("Encoded event: ", var_to_str(event), "\n", "Decoded event: ", var_to_str(event2))


func _physics_process(delta:float) -> void:
	if is_mobile:
		movement.process(delta)


func hurt(damage:Damage) -> void:
	var update = CharHudUpdate.new()
	update.got_damage = damage
	emit_signal(&'character_hud_update', update)

	mouth.stream = voice.hurt
	mouth.play()

	print("Character is hurt!")


func heal(health: int) -> void:
	var update = CharHudUpdate.new()

	update.got_healing = health
	emit_signal(&'character_hud_update', update)


func current_character_updated(new: CharacterBody3D, old: CharacterBody3D) -> void:
	if new == self:
		first_person = true
		camera.set_current(true)
#	else: # this is probably redundant
#		camera.clear_current()
