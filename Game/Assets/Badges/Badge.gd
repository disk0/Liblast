extends Resource
class_name CharacterBadge

@export var display_name : String
@export_multiline var description : String
@export var texture : Texture2D
@export var priority : int # lower values have higher priority
