extends Node
# All possible badges in the game

# APPEND ONLY! modifying this enum's order will result in corrupted user data!
enum Badge {
	ERROR,
	BOT,
	DEV,
	CON,
	SUP,
	ADM,
	MOD,
	PRE_ALPHA,
	ALPHA,
	BETA,
	DAY1,
}

# mapping enum values to resources

@onready var resources = {
	Badge.ERROR : load("res://Assets/Badges/Resources/Error.tres"),
	Badge.BOT : load("res://Assets/Badges/Resources/Bot.tres"),
	Badge.DEV : load("res://Assets/Badges/Resources/Developer.tres"),
	Badge.CON : load("res://Assets/Badges/Resources/Contributor.tres"),
	Badge.SUP : load("res://Assets/Badges/Resources/Supporter.tres"),
	Badge.ADM : load("res://Assets/Badges/Resources/Admin.tres"),
	Badge.MOD : load("res://Assets/Badges/Resources/Moderator.tres"),
	Badge.PRE_ALPHA : load("res://Assets/Badges/Resources/PreAlphaTester.tres"),
	Badge.ALPHA : load("res://Assets/Badges/Resources/AlphaTester.tres"),
	Badge.BETA : load("res://Assets/Badges/Resources/BetaTester.tres"),
	Badge.DAY1 : load("res://Assets/Badges/Resources/DayOne.tres"),
}
