extends Node3D

var check_overlap = true

var owner_pid : int = -1:
	set(value):
		owner_pid = value
		if get_node("Explosion"):
			$Explosion.owner_pid = owner_pid

func _ready():
	for i in get_children(): # activate all top-level particle systems secondary ones should be parented to the primary ones
		if i is GPUParticles3D:
			i.emitting = true

	self.look_at(get_viewport().get_camera_3d().global_transform.origin)

func remove_overlap():
	if $Smoke:
		$Smoke.queue_free()
	$AnimationPlayer2.playback_speed = 16.0

func _on_area_3d_area_entered(area):
	if check_overlap:
		area.get_parent().remove_overlap()

func _on_check_overlap_timeout():
	check_overlap = false
