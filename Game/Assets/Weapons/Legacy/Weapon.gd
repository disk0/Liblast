extends Node3D
#class_name Weapon
					#Hand		Camera			Head		Player
#@onready var main =   get_tree().root.get_node("Main")
#@onready var hud =   		main.get_node("HUD")
#@onready var score_rank =   hud.get_node("ScoreRank")
var character : CharacterBody3D
#@onready var camera = get_parent().get_parent()
@onready var ejector = get_child(0).get_node("Ejector")
@onready var muzzle = get_child(0).get_node("Muzzle")
var animation_player : AnimationPlayer

@export var animation_speed : float

var casing = preload("res://Assets/Weapons/Handgun/Casing.tscn")
var flash = preload("res://Assets/Effects/MuzzleFlash.tscn")
var tracer = preload("res://Assets/Effects/BulletTracer.tscn")
var impact_bullet = preload("res://Assets/Effects/ImpactSparks.tscn")
var impact_explosion = preload("res://Assets/Effects/ImpactExplosion.tscn")
var flyby_sound = preload("res://Assets/Audio/BulletFlyBySoundPlayer.tscn")

enum WeaponType {HITSCAN, PROJECTILE, MELEE}

@export var weapon_type : WeaponType
@export var projectile_or_tracer_scene : PackedScene
@export var weapon_damage : int

# accuracy
var spread_min : float = 1
var spread_max : float = 64
var spread_lerp : float = 0.01
var spread_boost : float = 16
var spread : float = spread_min

#enum Trigger {TRIGGER_PRIMARY, TRIGGER_SECONDARY}

# Ammo
@export_range(1, 100) var max_ammo:int
@export_range(1, 100) var max_reload_ammo:int
var ammo : int
var reload_ammo : int

var is_current : bool:
	set(value):
		if is_current == value:
			print_debug("Weapon is_current beign set to it's previous value")
			return

		is_current = value
		self.visible = is_current

func _controller_event(event: CharCtrlEvent) -> void:
	#primary trigger
	if character.controls[Globals.CharCtrlType.TRIG_P].changed: # changed gets reset to false whenever we check it
		if character.controls[Globals.CharCtrlType.TRIG_P].enabled:
			#pass # trigger was just pulled
			print("Primary Trigger pulled")
			shoot()
		elif not character.controls[Globals.CharCtrlType.TRIG_P].enabled:
			print("Primary Trigger released")
			#pass # trigger was just released

	# secondary trigger
	if character.controls[Globals.CharCtrlType.TRIG_S].changed: # changed gets reset to false whenever we check it
		if character.controls[Globals.CharCtrlType.TRIG_S].enabled:
			pass # trigger was just pulled
		elif not character.controls[Globals.CharCtrlType.TRIG_S].enabled:
			pass # trigger was just released

	# reloading
	if character.controls[Globals.CharCtrlType.WEPN_R].changed:
		if character.controls[Globals.CharCtrlType.WEPN_R].enabled:
			pass # reload was just pressed

func process(delta):
	pass

func reset_ammo() -> void:
	ammo = max_ammo
	reload_ammo = max_reload_ammo
	# TODO
	# main.update_ammo_info(true)

func reload() -> void:
	if reload_ammo < 1:
		# No more ammo to reload
		return

	if ammo == max_ammo:
		# No need to reload
		return

	var filled_ammo:int = max_ammo - ammo
	if filled_ammo > reload_ammo:
		filled_ammo = reload_ammo

	reload_ammo -= filled_ammo
	ammo += filled_ammo
	# TODO
	# main.update_ammo_info(true)

func lower_ammo() -> void:
	ammo -= 1
	# TODO
	# main.update_ammo_info(false)

@rpc(any_peer, call_local, reliable) func shoot(spread_offset:=Vector3.ZERO):
	print("SHOT")
	lower_ammo()

	$"SFX/Shoot A".play()
	$"SFX/Shoot B".play()
	$"SFX/Shoot C".play()

	var flash_effect = flash.instantiate()
	get_parent().add_child(flash_effect)
	flash_effect.global_transform = muzzle.global_transform

	#animation_player.play("Shoot", 0, animation_speed)

	var space_state = get_world_3d().direct_space_state

	# TODO
	var from = $BulletSource.get_global_transform().origin
	var aim = - $BulletSource.get_global_transform().basis[0]
	var to = from + (aim * 1000) + spread_offset

	var physics_ray_query_parameters_3d = PhysicsRayQueryParameters3D.new()
	physics_ray_query_parameters_3d.from = from
	physics_ray_query_parameters_3d.to = to

	# physics_ray_query_parameters_3d.exclude = [player]

	var ray = space_state.intersect_ray(physics_ray_query_parameters_3d)

	if weapon_type == WeaponType.HITSCAN:
		var tracer_instance = projectile_or_tracer_scene.instantiate()

		get_tree().root.add_child(tracer_instance)
		tracer_instance.global_transform = muzzle.global_transform.looking_at(to)

		if Settings.get_var("render_casing"):
			var casing_instance = casing.instantiate()
			get_tree().root.add_child(casing_instance)

			casing_instance.global_transform = ejector.global_transform#approximating delta

			# TODO# TODO
			casing_instance.linear_velocity = ejector.global_transform.basis[1] * randf_range(3.2, 4.5) # + (player.velocity / 2)
			casing_instance.angular_velocity.y += randf_range(-10, 10)
			casing_instance.angular_velocity.x += randf_range(-10, 10)


#		if ray:
#			if is_multiplayer_authority(): # only do this on the attacker's local instance of the game
#				give_damage(ray['collider'], ray['position'], ray['normal'], 20, self.global_transform.origin, Damage.DamageType.BULLET, 1)

		### bullet flyby sounds

		if ray: # if we hit something - use that to evaluate the flyby sound
				to = ray['position']

		var flyby_camera = get_tree().get_root().get_camera_3d()

		# TODO
		# if flyby_camera == camera: # don't spawn flyby sound for the shooter
		# 	return

		var x := Vector3.ZERO
		var A = from
		var B = to
		var C = flyby_camera.global_transform.origin

		var d0 = (B - A).dot(A - C)
		var d1 = (B - A).dot(B - C)

		if d0 < 0 and d1 < 0:
			print("Firing away from the camera")
		elif d0 > 0 and d1 > 0:
			print("Bullet hit before passing by")
		else:
			var X = d0/(d0-d1)
			var flyby = flyby_sound.instantiate()
			get_tree().root.add_child(flyby)
			flyby.global_transform.origin = A + X * (B - A)

		# TODO: spawn

	elif weapon_type == WeaponType.PROJECTILE:
		var projectile_instance = projectile_or_tracer_scene.instantiate()

		projectile_instance.global_transform = muzzle.global_transform.looking_at(to)
		projectile_instance.source_position = $BulletSource.global_transform.origin
		projectile_instance.owner_pid = get_multiplayer_authority()
		projectile_instance.damage = weapon_damage
		get_tree().root.add_child(projectile_instance)

# TODO: replace this bunch of loose variables with the Damage.gd class instances
#func give_damage(target: Node, hit_position: Vector3, hit_normal: Vector3, damage: int, source_position: Vector3, push: float):
#	if target.has_method(&'take_damage'): # we've hit a player or something else - the ywill handle everything like effects etc.
#		target.rpc(&'take_damage', get_multiplayer_authority(), hit_position, hit_normal, damage, source_position, type, push)
#	else:
#		# TODO: take data from the material of the target and spawn an appropriate hit effect
#		var impact_vfx : Node = impact_bullet.instantiate()
#		get_tree().root.add_child(impact_vfx)
#
#		impact_vfx.global_transform.origin = hit_position
#		var result = impact_vfx.look_at(hit_position + hit_normal)
#
#		if not result: # if the look_at failed (as it will on floors and ceilings) try another approach:
#			impact_vfx.look_at(hit_position + hit_normal, Vector3.LEFT)
#
#		impact_vfx.rotate(hit_normal, randf_range(0, PI * 2))

func trigger(index: int, active: bool) -> void:
	if index == 0 and active and animation_player.is_playing() == false:
		spread = min(spread + spread_boost, spread_max)

		var spread_offset = Vector3.ZERO

		spread_offset.x = randf_range(-1,1)
		spread_offset.y = randf_range(-1,1)
		spread_offset.z = randf_range(-1,1)

		spread_offset = spread_offset.normalized() * randf_range(spread_min, spread)

		rpc(&'shoot', spread_offset)

func find_aplayer(node:Node) -> AnimationPlayer:
	var x
	for i in node.get_children():
		if i is AnimationPlayer:
			x = i
			break
		elif i.get_child_count() > 0:
			x = find_aplayer(i)
			if x:
				break
	return x

func _ready() -> void:
	assert(projectile_or_tracer_scene != null, "projectile_or_tracer_scene is null!")
	animation_player = find_aplayer(self)
