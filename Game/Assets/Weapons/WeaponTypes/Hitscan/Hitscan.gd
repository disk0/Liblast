extends "res://Assets/Weapons/WeaponTypes/ShootingWeapon/ShootingWeapon.gd"

@export
var penetrating : bool = false

# Returns list of players shot
func cast_ray(from : Vector3, to : Vector3):
	var hits = []
	var exclude = [character]
	
	while true:
		var space_state = get_world_3d().direct_space_state
		
		var physics_ray_query_parameters_3d = PhysicsRayQueryParameters3D.new()
		
		physics_ray_query_parameters_3d.from = from
		physics_ray_query_parameters_3d.to = to
		physics_ray_query_parameters_3d.exclude = exclude
		
		var ray = space_state.intersect_ray(physics_ray_query_parameters_3d)
		
		if ray == {}:
			return hits
		if not penetrating:
			hits.append(ray)
			return hits
		if ray.collider is Character:
			hits.append(ray)
			physics_ray_query_parameters_3d.from = ray.position
			exclude.append(ray.collider)
			physics_ray_query_parameters_3d.exclude = exclude
		else:
			return hits

func generate_damage(hit):
	var damage = preload("res://Assets/Weapons/Damage/DamageShot.gd").new()

	damage.attacker = character
	damage.source_position = $Barrels/Barrel1/ProjectileSpawner.to_global(Vector3(0.0, 0.0, 0.0))
	damage.hit_position = hit.position
	damage.damage_amount = 20

	hit.collider.hurt(damage)

func shoot():
	var spawner = $Barrels/Barrel1/ProjectileSpawner
	var from = spawner.to_global(Vector3(0.0, 0.0, 0.0))
	var to = spawner.to_global(Vector3(-1000.0, 0.0, 0.0))

	var hits = cast_ray(from, to)

	for hit in hits:
		if hit.collider is Character:
			generate_damage(hit)

	$ShootSound.play()

func trigger_primary_press():
	shoot()
