extends Node3D
class_name Weapon

var character : CharacterBody3D

func generate_damage(player):
	pass

func hit_player(player):
	var damage = generate_damage(player)
	player.apply_damage(damage)

func process(delta):
	pass

# Shoot
var trigger_primary: bool = false:
	get:
		return trigger_primary
	set(value):
		if trigger_primary != value:
			trigger_primary = value
			if value:
				trigger_primary_press()
			else:
				trigger_primary_release()

func trigger_primary_press():
	pass

func trigger_primary_release():
	pass

# Aim down sights
var trigger_secondary: bool = false:
	get:
		return trigger_secondary
	set(value):
		if trigger_secondary != value:
			trigger_secondary = value
			if value:
				trigger_secondary_press()
			else:
				trigger_secondary_release()

func trigger_secondary_press():
	pass

func trigger_secondary_release():
	pass

# Reload
var reload: bool = false:
	get:
		return reload
	set(value):
		if reload != value:
			reload = value
			if value:
				reload_press()
			else:
				reload_release()

func reload_press():
	pass

func reload_release():
	pass

func _controller_event(event: CharCtrlEvent) -> void:
	#primary trigger
	if character.controls[Globals.CharCtrlType.TRIG_P].changed: # changed gets reset to false whenever we check it
		if character.controls[Globals.CharCtrlType.TRIG_P].enabled:
			#pass # trigger was just pulled
			trigger_primary_press()
		elif not character.controls[Globals.CharCtrlType.TRIG_P].enabled:
			trigger_primary_release()
			#pass # trigger was just released

	# secondary trigger
	if character.controls[Globals.CharCtrlType.TRIG_S].changed: # changed gets reset to false whenever we check it
		if character.controls[Globals.CharCtrlType.TRIG_S].enabled:
			pass # trigger was just pulled
		elif not character.controls[Globals.CharCtrlType.TRIG_S].enabled:
			pass # trigger was just released

	# reloading
	if character.controls[Globals.CharCtrlType.WEPN_R].changed:
		if character.controls[Globals.CharCtrlType.WEPN_R].enabled:
			pass # reload was just pressed
