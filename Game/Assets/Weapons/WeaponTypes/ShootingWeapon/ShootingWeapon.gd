extends "res://Assets/Weapons/WeaponTypes/Generic/Weapon.gd"

@onready var barrel = $Barrels/Barrel1
@onready var slide = $Slides/Slide1
@onready var magazine = $Magazines/Magazine1

func try_shoot():
	var can_fire = barrel.fire()
	if can_fire:
		shoot()

func shoot():
	# This will be implemented in child classes
	printerr("Shooting is only implemented in child classes")
