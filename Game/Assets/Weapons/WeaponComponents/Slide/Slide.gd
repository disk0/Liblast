extends Node3D
class_name Slide

signal slide_returned

@export_range(0.0, 2.0)
var rollback_time = 0.2

@export_node_path
var magazine_path

@onready
var magazine : Magazine = get_node(magazine_path)

func fire():
	if $Timer.is_stopped():
		if not magazine or magazine.feed_into_slide():
			$Timer.start(rollback_time)
			return true
	return false

func on_slide_return():
	emit_signal("slide_returned")
