extends Node3D
class_name Magazine

@export_range(0, 255, 1)
var clip_size = 0 # 0 for infinite clip size

@onready var remaining_shots = clip_size

func reloaded():
	remaining_shots = clip_size

func feed_into_slide():
	if clip_size == 0:
		return true
	else:
		if remaining_shots > 0:
			remaining_shots -= 1
			return true
		return false
