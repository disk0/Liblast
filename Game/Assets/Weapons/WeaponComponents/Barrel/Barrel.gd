extends Node3D
class_name Barrel

@export_range(0.0, 0.5)
var inaccuracy = 0.0


@export_node_path
var slide_path

@onready
var slide : Slide = get_node(slide_path)

func random_inaccuracy():
	# TODO: turn ProjectileSpawner randomly
	pass

func fire():
	if slide.fire():
		return true
	return false
