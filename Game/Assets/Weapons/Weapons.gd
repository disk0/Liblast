class_name Weapons

#enum WeaponSlot {
#	PRIMARY,
#	SECONDARY,
#	MELEE,
#}

enum Weapon {
	NONE,
	HITSCAN,
}

const WeaponScenePaths = {
	Weapon.NONE : null,
	Weapon.HITSCAN : "res://Assets/Weapons/WeaponTypes/Hitscan/Hitscan.tscn",
}
