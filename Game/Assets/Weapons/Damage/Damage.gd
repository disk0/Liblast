class_name Damage

var damage_amount : int
var damage_type : DamageType

func kill_message():
	return "Player generically died"

#enum DamageType {
#	NONE, # unknown damage
#	BULLET, # firearms
#	ENERGY, # plasma, electricity, laser
#	EXPLOSION, # shrapnel, shockwaves etc
#	IMPACT, # fall damage etc.
#	DROWNING, # swimming too deep
#	HAZARD, # bottomless pits, lava etc.
#}

#var attacker: Node # damage can be dealt by characters or other objects
#var target: Node # damage might be dealth to not just characters
#var hit_position: Vector3 # location on target that was hit
#var hit_normal: Vector3 # normal to the hit surface
#var damage: int
#var source_position: Vector3
#var type: DamageType
#var push: float # will apply force based on hit_position
