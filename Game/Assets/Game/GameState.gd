extends Node3D
class_name GameState

signal map_loaded

@onready var spawner : MultiplayerSpawner = $CharacterSpawner
@onready var characters = $Characters

@export var map_path : String:
	set(value):

		if value == map_path:
			#push_warning("Trying to load the same map again")
			return

		prints("Changing map_path to", value, "on peer", MultiplayerState.peer.get_unique_id())

		map_path = value

		if map:
			unload_map()

		if not map_path.is_empty():
			load_map()

var map = null
var hud = null

#@onready var hud_resource = load("res://Assets/HUD/HUD.tscn")
#@onready var character_container_resource = load("res://Assets/Characters/CharacterContainer.tscn")


func _ready():
	set_process(false)


func _process(delta):
	if ResourceLoader.load_threaded_get_status(map_path) == ResourceLoader.THREAD_LOAD_LOADED:
		map_loaded.emit()


func load_map():
	if has_map():
		unload_map()

	var map_resource : PackedScene

	print("Stating map loading...")
	print("Loader return code: ", ResourceLoader.load_threaded_request(map_path, "PackedScene", true))
	var time = Time.get_ticks_msec()
	set_process(true)
	await(map_loaded)
	set_process(false)
	time = Time.get_ticks_msec() - time
	prints("Map loaded in", time,"miliseconds")
	map_resource = ResourceLoader.load_threaded_get(map_path)
	map = map_resource.instantiate()
	map.name = "Map"
	add_child(map)
	map.connect("match_finished", on_match_finished)

	$HUD.show()


func has_map():
	return map != null


func unload_map():
	map.free()


func start_match():
	map.start_match()


# TODO: map loading/switching. For now, just start a new match
func on_match_finished():
	start_match()


# this is used to make clients take control over their characters that were spawned by the server for them
#@rpc(call_remote, any_peer, reliable) func take_over_character(character: Character):
#	prints("Taking over character", character.name, "by RPC call from", MultiplayerState.peer.get_packet_peer())
#	MultiplayerState.local_character = character
#	Globals.current_character = character


func spawn_character(pid: int):
	if multiplayer.is_server():
		print("Spawned character for ", pid)
		$CharacterSpawner.spawn(pid)
	else:
		print("Attempted spawning a character for ", pid, " on a client")



# spawn character - by default it will belong to the server
#func spawn_character(pid: int = MultiplayerPeer.TARGET_PEER_SERVER):
#	return
#	assert(map != null, "Cannot spawn a character when no map is loaded")

	# spawn HUD if there is none
#	if not hud:
#		hud = hud_resource.instantiate()
#		hud.name = "HUD"
#		add_child(hud)

#	if pid == MultiplayerPeer.TARGET_PEER_SERVER:
#		prints("Character", pid, "is local to server")
#		#take_over_character(character)
#	else:
#		prints("Character", pid, "belongs to a client.")
		#take_over_character.rpc_id(pid, character)

	# spawn character scene
#	var character_container = character_container_resource.instantiate()
#	var character = character_container.get_node("Character")
#
#	character_container.name = str(pid)
#	print_debug("Setting multiplayer authority for character ", pid)
#	character.set_multiplayer_authority(pid)
#	character.get_node("StateSynchronizer").set_multiplayer_authority(pid)
#	#await get_tree().create_timer(1).timeout
#	assert(character.get_multiplayer_authority() == pid, "Setting multiplayer authority failed!")
#	character.profile = MultiplayerState.user_character_profile
#	character.profile.display_name = str(pid)
#
#	prints("Adding character container", pid,"to scene tree")
#	characters.add_child(character_container)
