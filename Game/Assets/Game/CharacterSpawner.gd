extends MultiplayerSpawner

func _spawn_custom(owner_pid):
	if typeof(owner_pid) != TYPE_INT:
		push_error("Invalid player spawn received")
		return
	var character = load("res://Assets/Characters/Character.tscn").instantiate()
	character.set_character_owner(owner_pid)
	return character
#	return Node3D.new()
