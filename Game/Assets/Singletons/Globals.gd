extends Node
# Holds globally required enums and constants

enum Focus {MENU, GAME, CHAT, AWAY}
enum MultiplayerRole {NONE, CLIENT, SERVER, DEDICATED_SERVER}

enum CharCtrlType { # Character Control Type
	UNDEFINED, # undefined control type
	MOVE_F, # move_forward
	MOVE_B, # move_backward
	MOVE_L, # move_left
	MOVE_R, # move_right
	MOVE_S, # move_special
	MOVE_J, # move_jump
	TRIG_P, # trigger_primary
	TRIG_S, # trigger_secondary
	WEPN_1, # weapon_1
	WEPN_2, # weapon_2
	WEPN_3, # weapon_3
	WEPN_L, # weapon_last
	WEPN_R, # weapon_reload
	V_ZOOM, # view_zoom
}

#enum MaterialType { NONE,
#					CONCRETE,
#					METAL,
#					WOOD,
#					GLASS,
#					WATER,
#					}

#const NET_SERVER : String = "libla.st"
const NET_SERVER : String = "localhost"
const NET_PORT : int = 12597
const NET_PEER_LIMIT = 16

#const INFRA_SERVER : String = "localhost"
const INFRA_SERVER : String = "unfa.xyz"
const INFRA_PORT : int = 12599

signal FocusChanged(new_focus: Focus)
signal CurrentCharacterChanged(new_character:CharacterBody3D, old_character: CharacterBody3D)


var focus: Focus:
	set(value):
		prints("Focus changed to", value)
		if value == self.focus:
			return

		focus_previous = focus
		focus = value
		FocusChanged.emit(focus)

		# make mouse cursor visible only in MENU focus
		if value == Focus.MENU:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

		# only allow controlling the character in GAME focus
		if MultiplayerState.role != Globals.MultiplayerRole.NONE:
			if MultiplayerState.local_character:
				if value == Focus.GAME:
					MultiplayerState.local_character.is_controllable = true
				else:
					MultiplayerState.local_character.is_controllable = false


var focus_previous: Focus

# this is the character currently followed by the first person camera and HUD
# it's NOT the same as MultiplayerState's local_character, but usually current_character will point to local_character
var current_character: CharacterBody3D = null:
	set(value):
		if value == current_character:
			print_debug("Attempting to set exisitng current_character; skipping")
			return

		#								      new ↓           ↓ old
		emit_signal(&'CurrentCharacterChanged', value, current_character)
		print_debug("Changed current character from ", current_character, " to ", value, " on peer ", MultiplayerState.peer.get_unique_id())
		current_character = value

var team_colors = [
	Color.WHITE,
	Color.html("7700ff"), # purple
	Color.html("c6ff06"), # lime
]
