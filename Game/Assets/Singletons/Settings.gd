extends Node

### This singleton manages global game settings this is a bit like cvars in idtech engines

# SETTINGS

var version: String

signal var_changed(var_name, value)

var settings = {} # current game settings

var settings_dir = "user://settings/"
var settings_file = settings_dir + "settings.liblast"
var settings_last = {} # copy of last settings for undo

var presets_dir = "res://Settings/"
var presets = {} # a dictionary of var_name presets. preset : settings{}

var dirty = false # have the settings been altered?

# DEFAULTS

var settings_default = {
#	'player_name' = "player",
#	'player_color' = Color.GRAY.to_html(),
#	'player_uuid' = OS.get_unique_id(),
	'player_first_run' = true,
#	'player_play_time' = 0.0,
#	'player_games_played' = 0,
#	'player_games_won' = 0,
	'auth_enbled' = false,
	'auth_enabled_remember' = false,
	'auth_username' = "",
	'auth_username_remember' = false,
#	'user_avatar_hash' = PackedByteArray(),
#	'player_account_login' = null, # encrypted
#	'player_account_password_remember' = true,
#	'player_account_password' = null, # encrypted
#	'player_account_last_login' = null, # timestamp
#	'input_mouse_sensitivity' = 0.5,
#	'network_game_host' = 'libla.st',
#	'network_game_port' = 12597,
#	'network_lobby_host' = 'libla.st',
#	'network_lobby_port' = 12598,
#	'network_auth_host' = 'libla.st',
#	'network_auth_port' = 12599,
	'display_fullscreen' = true,
	'display_window_size' = Vector2(1280,720),
	'display_vsync_enabled' = true,
	'display_vsync_mode' = 2, # Disabled, Enabled, Adaptive, Mailbox
	'render_scale' = 3, # 3D Scaling: 0.25, 0.5, 1.0, 2.0, 4.0
	'render_fps_limit' = 0, # 0 means unlimited
	'render_msaa' = 0, # none, 2x, 4x, 8x
	'render_ssaa' = 0, # none, fxaa
	'render_debug_mode' = 0,
	'render_debanding_enabled' = false,
	'render_ssrl_enabled' = false,
	'render_ssrl_amount' = 0.25,
	'render_ssrl_limit' = 0.18,
	'render_fsr_enable' = false,
	'render_glow_enabled' = true,
	'render_glow_quality' = 1.0,
	'render_refprobes_enabled' = true,
	'render_ssr_enabled' = false,
	'render_ssr_quality' = 1.0,
	'render_ssao_enabled' = false,
	'render_ssao_quality' = 1.0,
	'render_ssil_enabled' = false,
	'render_ssil_quality' = 1.0,
	'render_particles_extra' = false,
	'render_casing' = true,
	'host_name' = "Liblast Server",
	'host_welcome_message' = "Welcome to Liblast Server! Have fun!",
	'host_peer_limit' = 32,
	'host_peer_require_auth' = false, # players need to be authenticated to join
	}


func check_version():
	# update game build version, if we're running in the editor
	if Engine.get_version_info():
		OS.execute('bash', ["./version.sh"])

	var file = File.new()
	file.open("res://version", File.READ)
	version = file.get_as_text()
	file.close()

	var out = []
	OS.execute('hostname', [], out)

	return out


func _ready() -> void:
#	print_debug("Initialising a new settings manager: ", self)
	# ensure the settings directory exists
	var dir = Directory.new()
	if not dir.dir_exists(settings_dir):
		dir.make_dir_recursive(settings_dir)

	var file = File.new()

	settings = Dictionary()

	if file.file_exists(settings_file):
#		print_debug("settings file exists, loading")
		var settings_loaded = load_settings(settings_file) as Dictionary

		for i in settings_default.keys():
			if settings_loaded.has(i):
				settings[i] = settings_loaded[i]
			else:
				settings[i] = settings_default[i]

	call_apply_all()


# SAVE/LOAD


# TODO: Unify the type of the return value
func save_settings(settings, filename, force = false):
	if not dirty and not force:
#		print_debug("Attempted to save unmodified settings, skipping")
		return
#	elif not dirty and force:
#		print_debug("Forced saving unmodified settings")
#	else:
#		print_debug("Saving dirty settings")

	var json = JSON.new()
	var json_string = json.stringify(settings)

	var file = File.new()
	file.open(filename, File.WRITE)
	file.store_string(json_string)
	if file.get_error():
		return file.get_error()
	file.close()

	dirty = false


func load_settings(filename):
	var file = File.new()
	var json_string : String
	file.open(filename, File.READ)
	json_string = file.get_as_text()
	if file.get_error():
		return null
	file.close()

	var json = JSON.new()
	json.parse(json_string)
	return json.get_data()


# SET/GET


func set_var(var_name: String, value: Variant) -> void:
	if not dirty:
		dirty = true
	settings[var_name] = value
	emit_signal(&'var_changed', var_name, value)
	call_apply_var(var_name)
	save_settings(settings, settings_file)
#	print_debug("Variable ", var_name, " was set to ", value)


func get_var(var_name: String) -> Variant: # return a given var_name
	return settings.get(var_name)


# APPLY


func call_apply_var(var_name: String) -> void: # call function corresponding to the given var_name
	var apply_method: StringName = StringName("apply_" + var_name)
	if has_method(apply_method):
		call(apply_method, settings[var_name])
#	else:
#		printerr("Settings var_name ", var_name, " has no apply method")


func call_apply_all(): # apply all current settings
	for key in settings.keys():
		call_apply_var(key)


func load_preset(preset: String) -> void: # load var_names from a preset
	settings_last = settings
	settings = presets[preset]


func restore_last() -> void:
	settings = settings_last

### VARIABLE APPLY FUNCTIONS

#func apply_player_name(value:String) -> void:
#		print_debug("Setting player name to ", value)


func apply_display_fullscreen(value:bool) -> void:
	if value:
		get_viewport().mode = Window.MODE_FULLSCREEN
	else:
		get_viewport().mode = Window.MODE_WINDOWED
#		get_viewport().mode = Window.MODE_MAXIMIZED


func apply_render_fps_limit(value) -> void:
	prints("Limiting framerate to", value,"FPS")
	Engine.target_fps = value

func apply_render_scale(value) -> void:
	var exp_value = pow(2, value) * 0.125
#	print_debug("Applying render scale: ", exp_value)
	get_viewport().scaling_3d_scale = exp_value


func apply_display_vsync_mode(value) -> void:
#	print_debug("Applying Vsync mode: ", value)
	DisplayServer.window_set_vsync_mode(value)


func apply_render_debug_mode(value) -> void:
	get_viewport().debug_draw = value


func apply_render_fsr_enabled(value) -> void:
	get_viewport().scaling_3d_mode = Viewport.SCALING_3D_MODE_FSR if value else Viewport.SCALING_3D_MODE_BILINEAR
