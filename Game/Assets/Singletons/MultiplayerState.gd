extends Node
# Holds global multiplayer peer and role

# for signalling when auth status has changed
signal auth_changed(enabled: bool)

#signal ServerStarted(error)

var game_state: GameState = null # stores reference to active game state if present

var local_character: Character = null

var uptime = 0 # seconds
const respawn_delay : float = 3 # seconds
const reset_delay : float = 10 # seconds
var spawn_queue = {}
var reset_at : float = -1

var game_score_limit = 10 #15

var auth_enabled : bool:
	set(value):
		auth_enabled = value
		if value == true:
			emit_signal(&"auth_changed", true)
		else:
			emit_signal(&"auth_changed", false)

var auth_username : String
var auth_tokens : Array # auth tokens

@onready var user_character_profile := CharacterProfile.new()

@onready var peer := ENetMultiplayerPeer.new()

var role := Globals.MultiplayerRole.NONE#:
#	set(new_role):
#		role = new_role
#		print("Multiplayer Role changed to ", Globals.MultiplayerRole.keys()[new_role])

func spawn_game_state():
	if not game_state:
		print("Spawning game state")
		game_state = load("res://Assets/Game/GameState.tscn").instantiate()
		print("Game state instantiated")
		game_state.name = "GameState"
		get_tree().root.add_child(game_state)
		print("Game state added to scene tree")
	else:
		push_warning("Trying to spawn a second GameState")


func free_game_state():
	if game_state:
		game_state.queue_free()
	else:
		push_warning("Trying to free a non-existing GameState")


func start_server(role=Globals.MultiplayerRole.SERVER):
	if role == Globals.MultiplayerRole.DEDICATED_SERVER:
		print("Starting dedicated game server...")
		AudioServer.set_bus_mute(0, true)  # Mute sound
	else:
		print("Starting game server...")

	var err = peer.create_server(Globals.NET_PORT, Globals.NET_PEER_LIMIT)

	if err == OK:
		self.role = role
		get_tree().get_multiplayer().multiplayer_peer = peer
		print("Started server")
#		set_physics_process(true)

		spawn_game_state()
		await(game_state.map_loaded) # wait for the map
		game_state.spawn_character(1) # spawn server's local character
		Globals.focus = Globals.Focus.GAME

		#ServerStarted.emit(OK)

	else:
		prints("Cannot start server:", error_string(err))
		role = Globals.MultiplayerRole.NONE
		#ServerStarted.emit(err)
	return err


func stop_server():
	assert(multiplayer.is_server(), "Trying to stop server, while not being a server")

	peer.close_connection()
	get_tree().get_multiplayer().multiplayer_peer = null
	role = Globals.MultiplayerRole.NONE

	free_game_state()

	Globals.focus = Globals.Focus.MENU

#	set_physics_process(false)


func start_client(host: String):
	var err = peer.create_client(host, Globals.NET_PORT)

	if err == OK:
		role = Globals.MultiplayerRole.CLIENT
		get_tree().get_multiplayer().multiplayer_peer = peer
		print("Started client")
		spawn_game_state()

		Globals.focus = Globals.Focus.GAME

#		rpc(&"request_game_join")
#		set_physics_process(true)
#		peer.set_target_peer(MultiplayerPeer.TARGET_PEER_SERVER)
#		var request = ["join_game"]
#		peer.put_var(request)

	else:
		prints("Cannot start client:", error_string(err))
		role = Globals.MultiplayerRole.NONE

	return err


func stop_client():
	assert(role == Globals.MultiplayerRole.CLIENT, "Trying to stop client but the role is not client")
	peer.close_connection()
	get_tree().get_multiplayer().multiplayer_peer = null
	role = Globals.MultiplayerRole.NONE

	free_game_state()

	Globals.focus = Globals.Focus.MENU
#	set_physics_process(false)


#func _ready():
#	var rpc_config =\
#		{
#			rpc_mode = MultiplayerAPI.RPC_MODE_ANY_PEER,
#			transfer_mode = MultiplayerPeer.TRANSFER_MODE_RELIABLE,
#			call_local = false,
#			channel = 0,
#		}
#
#	rpc_config(&"request_game_join", rpc_config)


func _physics_process(delta):
	return

#	if Globals.MultiplayerRole.CLIENT:
#		request_game_join.rpc() # this is the only eorking syntax to do rpc calls.

#	print("Packet error: ", error_string(peer.get_packet_error()))

@rpc(any_peer, reliable, call_remote) func request_game_join():
	prints("Recieved join game request from", peer.get_packet_peer())


func _on_connection_succeeded():
	pass
#	request.rpc_id(MultiplayerPeer.TARGET_PEER_SERVER, )

func _on_connection_failed():
	pass

func _on_server_disconnected():
	pass

func _on_peer_connected(pid):
	prints("Peer connected:", pid)

	# only server cna spawn characters - the rest is handled by the multiplayer spawner nodes
	if MultiplayerState.role in [Globals.MultiplayerRole.SERVER, Globals.MultiplayerRole.DEDICATED_SERVER]:
		game_state.spawn_character(pid)

func _on_peer_disconnected(pid):
	prints("Peer disconnected:", pid)

#@rpc(any_peer, reliable, call_remote) func set_map(map_path: String):
#	prints("Set map called")
#	game_state.map_path = map_path

func _ready():
	peer.connection_succeeded.connect(_on_connection_succeeded)
	peer.connection_failed.connect(_on_connection_failed)
	peer.server_disconnected.connect(_on_server_disconnected)
	peer.peer_connected.connect(_on_peer_connected)
	peer.peer_disconnected.connect(_on_peer_disconnected)
