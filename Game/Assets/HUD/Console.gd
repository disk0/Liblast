extends Control

@onready var editor = $VBoxContainer/Editor
@onready var history = $VBoxContainer/History

enum PromptStatus {UNKNOWN, INVALID, COMMAND, GET, SET}

const CONSOLE_SIZE = 0.75

var commands = [
	'help',
	'quit',
	'coms',
	'vars',
	'clear',
	'kill',
	'killall',
	'god',
	'noclip',
]

const colors = {
	PromptStatus.UNKNOWN : Color.GHOST_WHITE,
	PromptStatus.INVALID : Color.RED,
	PromptStatus.COMMAND : Color.MEDIUM_SPRING_GREEN,
	PromptStatus.GET : Color.PALE_GREEN,
	PromptStatus.SET : Color.DEEP_SKY_BLUE,
}

var prompt_status: PromptStatus:
	set(value):
		prompt_status = value

		var prompt_color = colors[prompt_status]
		editor.set('theme_override_colors/font_color', prompt_color)

var prompt
var argument

var prompt_history = []
var prompt_history_index = null

var active := true:
	set(value):
		if active == value: # the value was changed to itself
			return

		active = value

		if Globals.current_character:
			Globals.current_character.is_controllable = ! active

		if active:
			editor.grab_focus()
		else:
			editor.release_focus()

func _ready() -> void:
	prompt_status = PromptStatus.UNKNOWN
	command_help(null)
	active = false
	#anchor_bottom = 0

func _process(delta) -> void:
	if active:
		anchor_bottom = lerp(anchor_bottom, CONSOLE_SIZE, min(delta * 16, 1))
		anchor_top = anchor_bottom - CONSOLE_SIZE
	else:
		anchor_bottom = lerp(anchor_bottom, -0.01, min(delta * 16, 1))
		anchor_top = anchor_bottom - CONSOLE_SIZE

func toggle_console() -> void:
	# ensure the console is always drawn on top of everything else
	var root = get_tree().root
	var child_count = root.get_child_count()
	root.move_child(self, child_count)

	if Input.is_action_just_pressed(&'console'):
		active = ! active
		get_tree().get_root().set_input_as_handled()


func _input(event):
	if active:
		if prompt_history.size() > 0:

			if Input.is_action_just_pressed(&'ui_up'):
				if prompt_history_index == null:
					prompt_history_index = prompt_history.size()
				elif prompt_history_index > 0:
					prompt_history_index -= 1

			elif Input.is_action_just_pressed(&'ui_down'):
				if prompt_history_index == null or prompt_history_index == prompt_history.size():
					return
				elif prompt_history_index < prompt_history.size():
					prompt_history_index += 1

			editor.text = prompt_history[prompt_history_index]
			prompt_status = PromptStatus.UNKNOWN
			get_viewport().set_input_as_handled()

func _unhandled_input(_event) -> void:
	if not active:
		toggle_console()

	if Input.is_action_just_pressed(&'clear') and active:
		editor.text = ''
		prompt_status = PromptStatus.UNKNOWN
		get_viewport().set_input_as_handled()

func split_prompt(text:String) -> void:
	prompt = text.get_slice(' ', 0)
	var split = text.split(' ', false, 1)

	if split.size() == 2:
		argument = split[1]
	else:
		argument = null

func validate_prompt(text: String) -> void:
	# separate command and argument, rest is dropped
	split_prompt(text)

	# check if the prompt matches command
	if prompt in commands and has_method(StringName('command_' + prompt)):
		prompt_status = PromptStatus.COMMAND
	# check if we're trying to get a variable
	elif prompt in Settings.settings.keys() and argument == null:
		prompt_status = PromptStatus.GET
	# check if we're trying to set a variable
	elif prompt in Settings.settings.keys() and argument != null:
		prompt_status = PromptStatus.SET
	else:
		prompt_status = PromptStatus.UNKNOWN

func _on_editor_text_submitted(new_text: String) -> void:
	if prompt_status == PromptStatus.UNKNOWN:
		prompt_status = PromptStatus.INVALID

	if prompt_status == PromptStatus.INVALID:
		return

	if prompt_status == PromptStatus.COMMAND:
		call(StringName('command_' + prompt), argument)
		prompt_history.append(prompt)

	elif prompt_status == PromptStatus.GET:
		print(prompt)
		print(Settings.get_var(prompt))
		history.newline()
		history.append_text("[color=" + colors[prompt_status].to_html() + "]> " + prompt + " is " + var_to_str(Settings.get_var(prompt)) + "[/color]")

	elif prompt_status == PromptStatus.SET:
		Settings.set_var(prompt, str_to_var(argument))
		history.newline()
		history.append_text("[color=" + colors[prompt_status].to_html() + "]> " + prompt + " is now " + var_to_str(Settings.get_var(prompt)) + "[/color]")

	editor.text = ""
	prompt_status = PromptStatus.UNKNOWN

func _on_editor_text_changed(new_text: String) -> void:
	if active:
		toggle_console()
		if not active:
			editor.text = editor.text.rstrip('`')
			editor.caret_column = editor.text.length()
	validate_prompt(new_text)
	get_tree().get_root().set_input_as_handled()

func _on_editor_focus_exited():
	# this prevents loosing focus when the console is active
	if active:
		editor.grab_focus()


func command_help(argument) -> void:
	history.newline()
	history.newline()

	if argument == null:
		history.append_text(
'''[b]--- Liblast Console Help ---[/b]

- Key bindings:

	[b]TILDA[/b] toggles the Console
	[b]CTRL+BACKSPACE[/b] clears the current prompt
	[s]UP/DOWN cycles through prompt history[/s]

- Commands

	To view this help text, type "help"
	To list available commands, type "coms"
	To list available variables, type "vars"

	To get help for a given command, type "help" and it\'s name

	To get a variable, type it\'s name
	To set a variable, type it\'s name followed by the new value - mind the formatting

- Value formatting:

	Strings must be encapsulated in "quotes"
''')
	else:
		match argument:
			'clear' : history.append_text('clear removes all text from the console')
			'quit' : history.append_text('quit closes the game')

func command_coms(argument) -> void:
	history.newline()
	history.newline()
	history.append_text("Available [b]commands[/b]:")
	history.newline()
	history.append_text(var_to_str(commands))

func command_vars(argument) -> void:
	history.newline()
	history.newline()
	history.append_text("Available [b]variables[/b]:")
	history.newline()
	history.append_text(var_to_str(Settings.settings.keys()))

func command_clear(argument) -> void:
	history.clear()

func command_quit(argument) -> void:
	history.newline()
	history.append_text("Exiting Liblast...")
	get_tree().quit()

func command_kill(argument) -> void:
	history.newline()
	history.append_text("Killing player... you ok there?")
	if Globals.current_character:
		Globals.current_character.die()

func command_killall(argument) -> void:
	history.newline()
	history.append_text("Killing everyone... I hope you're happy.")
	for i in get_tree().get_nodes_in_group(&'Characters'):
		i.die()

func command_god(argument) -> void:
	history.newline()
	history.append_text("Nice try. Godmode isn't implemented yet.")

func command_noclip(argument) -> void:
	history.newline()
	history.append_text("Sorry. Noclip isn't implemented yet.")
