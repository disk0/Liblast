extends Control

# TODO - decouple:
@onready var main = get_tree().get_root().get_node("Main")

var pain: float = 0:
	set(value):
#		$Overlays/Damage.material.set('shader_param/Damage', value)
		if value < 0.1:
			$Overlays/DamageStatic.modulate.a = 0
		else:
			$Overlays/DamageStatic.modulate.a = clamp(value, 0, 0.1)
	get:
#		return $Overlays/DamageStatic.material.get('shader_param/Damage')
		return $Overlays/DamageStatic.modulate.a

# manage HUD visibility
func _on_focus_changed(focus):
	if focus in [Globals.Focus.GAME, Globals.Focus.CHAT]:
		show()
	else:
		hide()

# recieves the signal from Globals
func _on_current_character_changed(new_character: CharacterBody3D, old_character: CharacterBody3D) -> void:
	# disconnect the old character (if exists)
	if old_character:
		old_character.disconnect(&'character_hud_update', character_hud_update)

	# connect the new character (if exists)
	if new_character:
		new_character.connect(&'character_hud_update', character_hud_update)

func game_over(winner) -> void:
	scoretab(true, winner)


func damage(hp) -> void:
	print("HUD damage ", hp)
	pain += hp / 20


func hide() -> void:
	$Crosshair.hide()
	$Chat.hide()


func show() -> void:
	$Crosshair.show()
	$Chat.show()


func _ready() -> void:
	Globals.CurrentCharacterChanged.connect(_on_current_character_changed)
	Globals.FocusChanged.connect(_on_focus_changed)

	pain = 0

func get_pid_info(pid: int) -> Dictionary:
		var player_name = main.player_list.get_item(pid).name
		var player_color = main.player_list.get_item(pid).color
		var ping = main.player_list.get_item(pid).ping
		if not ping:
			ping = "?"
		var packet_loss = main.player_list.get_item(pid).packet_loss
		if not packet_loss:
			packet_loss = "?"
		return {
				'name' = player_name,
				'color' = player_color,
				'ping' = ping,
				'loss' = packet_loss,
				}


func update_scoretab() -> void:
	$ScoreTable/VBoxContainer/ScoreTab.text = ''

	var scores = []

	for pid in main.player_list.players.keys():
		scores.append(main.player_list.get_item(pid).score)

	scores.sort()
	scores.reverse()

	var done = []

	for score in scores:
		for pid in main.player_list.players.keys():
			if main.player_list.get_item(pid).score == score:
				if pid not in done:
					var info = get_pid_info(pid)

					$ScoreTable/VBoxContainer/ScoreTab.text += "[b]%s [color=\"%s\"]%s[/color][/b]   (%s ms · %s)\n" % [
						str(score),
						Color(info['color']).to_html(),
						info['name'],
						str(info['ping']),
						str(info['loss'])
					]

					done.append(pid)

func scoretab(show: bool, winner = null) -> void:
	if show:
		update_scoretab()
		$ScoreTable.show()
	else:
		$ScoreTable.hide()

	if winner:
		$ScoreTable/VBoxContainer/Header.text = "Match over! Player %s won!" % main.player_list.get_item(winner).name
	else:
		$ScoreTable/VBoxContainer/Header.text = "Playing deathmatch until %s kills" % str(MultiplayerState.game_score_limit)


# This function has to be split up
# You don't need to update everything if you just want to update the health
func update_hud() -> void:
	# Health
	update_health_bar()
	check_scoreboard()

func check_scoreboard() -> void:
	if main.player_list.players.size() <= 1:
		return # if we're the sole player in the server, stop here

	var pid : int = get_tree().get_multiplayer().get_unique_id()
	var player = main.player_list.get_item(pid)

	# SCORE, RANK, GAP/LEAD
	get_node("ScoreRank").text = "SCORE: " + str(player.score)

	var score : int = player.score
	var scores = []

	for i in main.player_list.players.values():
		scores.append(i.score)

	scores.sort()
	scores.reverse()
	var rank = scores.find(score) + 1

	scores.remove_at(scores.find(score))
	scores.sort()
	scores.reverse()

	var lead = score - scores[0]

	get_node("ScoreRank").text = "SCORE: %s\nRANK: " % str(score)

	if lead > 0:
		get_node("ScoreRank").text	+= "%s\nLEAD: %s" % [str(rank), str(lead)]
	else:
		get_node("ScoreRank").text	+= "%s\nGAP: %s" % [str(rank), str(-lead)]

func update_health_bar() -> void:
	var pid : int = get_tree().get_multiplayer().get_unique_id()
	var player = main.player_list.get_item(pid)

	var health_bar = get_node("Stats").get_node("HealthBar")
	var health : int = player.health
	health_bar.value = health
	health_bar.get_node("Label").text = str(health) + "%"

func _process(delta) -> void:
	if not get_tree().get_multiplayer().has_multiplayer_peer(): # don't do anything if we're offline
		return

	if MultiplayerState.spawn_queue.has(get_tree().get_multiplayer().get_unique_id()):
		var countdown = MultiplayerState.spawn_queue[get_tree().get_multiplayer().get_unique_id()] - MultiplayerState.uptime
		$RespawnCountdown.text = "RESPAWNING IN %s SECONDS..." % str("%1.2f" % countdown)
		$RespawnCountdown.visible = true
	else:
		$RespawnCountdown.visible = false

	if $ScoreTable.visible: # update the scores every frame when player is watching the score table
		update_scoretab()

	# time heals pain
	if MultiplayerState.local_character != null: # alive
		pain *= 1 - delta
	else: # dead
		pain *= 1 - delta / MultiplayerState.respawn_delay


# this function recieves and processes updates from the current game character
func character_hud_update(update: CharHudUpdate) -> void:
#	print(var_to_str(update))
	if update.special != null:
		$Stats/JetpackBar.value = update.special
	$Stats/JetpackBar/Label.text = update.special_type + ": " + str(round(update.special * 100)) + "%"
