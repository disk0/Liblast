extends Node

func _ready():
	var args = OS.get_cmdline_args()

	#args.clear()

	print(args)
	for i in range(args.size()):
		match args[i]:
			"--host":
				var map_path = args[i+1]
				prints("Requested hosting a local game on map", map_path)
				$UI/PlayerAuthMenu._on_anonymous_pressed()
				await get_tree().create_timer(0.1).timeout # without these pauses the triggered UI actions don't work properly
				#_on_auth_menu_closed()
				$UI/MainMenu/TabContainer/Game.selected_map_path = map_path
				await get_tree().create_timer(0.1).timeout
				$UI/MainMenu/TabContainer/Game/CenterContainer/VBoxContainer/HBoxContainer3/Host.button_pressed = true

			"--join":
				var server = args[i+1]
				prints("Requested joining a game at", server)
				$UI/PlayerAuthMenu._on_anonymous_pressed()
				await get_tree().create_timer(0.1).timeout
				$UI/MainMenu/TabContainer/Game/CenterContainer/VBoxContainer/HBoxContainer/HostAddress.text = server
				await get_tree().create_timer(0.1).timeout
				$UI/MainMenu/TabContainer/Game/CenterContainer/VBoxContainer/HBoxContainer/Join.button_pressed = true
			"--test-launcher":
				get_tree().change_scene_to_file("res://Tests/MultiplayerTestLauncher.tscn")

	Globals.focus = Globals.Focus.MENU
	Globals.FocusChanged.connect(_on_focus_changed)


func _on_auth_menu_closed():
	print("PlayerAuth menu closed")
	$UI/MainMenu.show()
	$UI/MainMenu.menu_map = $UI/BackgroundMap

func _on_focus_changed(focus: Globals.Focus):
	if focus == Globals.Focus.MENU:
		$UI.show()
		#Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		$UI.hide()


func _unhandled_key_input(event: InputEvent):
	if event.is_action_pressed("ui_cancel"): # Escape

		# if the menu is open, try to bring the focus back to where it was before
		if Globals.focus == Globals.Focus.MENU and Globals.focus_previous != Globals.Focus.MENU:
			Globals.focus = Globals.focus_previous

		# open the menu
		elif Globals.focus != Globals.Focus.MENU:
			Globals.focus = Globals.Focus.MENU

		get_tree().root.set_input_as_handled()


# limit game FPS when the window is not active
func _notification(what: int) -> void:
	return # temporarily skip the following logic

	match what:
		NOTIFICATION_APPLICATION_FOCUS_OUT:
			Engine.target_fps = 1
#			mute_previous = get_mute()
#			set_mute(true)
#			if MultiplayerState.local_player:
#				local_player_focus_previous = focus
#				focus = Globals.Focus.AWAY
		NOTIFICATION_APPLICATION_FOCUS_IN:
			# `0` means "unlimited".
			Engine.target_fps = Settings.get_var("render_fps_limit")
#			set_mute(mute_previous)
#			if MultiplayerState.local_player:
#				focus = local_player_focus_previous
